package com.example.demo.reposotory;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.example.demo.model.User;

public interface UserRepository extends JpaRepository<User,Long> {

	@Query("SELECT u FROM User u WHERE u.email=?1 AND u.password=?2")
	public Optional<User> findUserByUsernameAndPassword(String email, String password);

}
