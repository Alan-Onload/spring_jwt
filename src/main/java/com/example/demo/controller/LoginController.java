package com.example.demo.controller;

import java.util.Date;
import java.util.Optional;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.example.demo.model.Token;
import com.example.demo.model.User;
import com.example.demo.service.UserService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/login")
public class LoginController {

	@Autowired
	private UserService user;
	@Value("${chave}")
	private String chave; 

	@PostMapping("/")
	public Token login(@RequestBody User u) {
		Token mytoken = new Token();
		
		//pesquisa no banco se o user existe
		Optional<User> usuario = user.pesquisarLogin(u.getEmail(),u.getPassword());
		
		//if existe gera o token
		if(usuario.isPresent()) {
		try {
			
		    Algorithm algorithm = Algorithm.HMAC256(this.chave);
		    String token = JWT.create()
		        .withIssuer("auth0")
		        .withIssuedAt(new Date()) 
	            .withExpiresAt(DateUtils.addHours(new Date(),2))
	            .withClaim("name",usuario.get().getName())
		        .sign(algorithm);
		    
		    mytoken.setId(usuario.get().getId());
		    mytoken.setNome(usuario.get().getName());
		    mytoken.setToken(token);
		    
		} catch (JWTCreationException exception){

			 exception.printStackTrace();
	
		 }
		};
		
		//retorna o objeto Token com id, nome , token
		return mytoken;
		

	}
}